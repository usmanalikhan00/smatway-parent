import {NgModule, ErrorHandler} from "@angular/core";
import {IonicApp, IonicModule, IonicErrorHandler} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';

import {MyApp} from "./app.component";

import {HomePage,
        LoginPage,
        StudentPerformance, 
        Profile,
        SingleAssignment,
        StudentActivity,
        StudentAssignments,
        Planner, 
        EventDetail,
        Inbox, 
        SingleChat, 
        SendMessage} from '../pages/pages-index.component'
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// calendar
import { NgCalendarModule  } from 'ionic2-calendar';
import { Ionic2RatingModule } from 'ionic2-rating';
import { StreamingMedia } from '@ionic-native/streaming-media';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    StudentPerformance,
    StudentActivity,
    SingleAssignment,
    Profile,
    Planner,
    EventDetail,
    StudentAssignments,
    Inbox, 
    SendMessage,
    SingleChat,
    
  ],
  imports: [
    BrowserModule,
    NgCalendarModule,
    Ionic2RatingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Inbox, 
    SendMessage,
    SingleChat,
    StudentActivity,
    SingleAssignment,
    StudentPerformance,
    MyApp,
    HomePage,
    LoginPage,
    Profile,
    Planner,
    EventDetail,
    StudentAssignments,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    StreamingMedia,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})

export class AppModule {
}
