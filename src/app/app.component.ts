import { Component, ViewChild } from "@angular/core";
import { Platform, Nav, Events } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { HomePage, LoginPage, Planner, Inbox } from "../pages/pages-index.component";
import { Storage } from '@ionic/storage';

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;
  loggedUser: any = null
  selectedClassName: any = null
  selectedClass: any = null

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public Events: Events,
    public _storage: Storage,
    public keyboard: Keyboard) {
    //*** Control Status Bar
    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Planner', component: Planner, icon: 'calendar'}
    ];
    Events.subscribe('user:created', (user, time) => {
      console.log('Welcome', user, 'at', time);
      this.loggedUser = user
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();


      //*** Control Keyboard
      // this.keyboard.disableScroll(true);

    });
  }

  goToInbox(){
    this.nav.setRoot(Inbox);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this._storage.remove('user')
    // window.location.reload
    this.nav.setRoot(LoginPage);

  }

}
