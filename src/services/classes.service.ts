import { Injectable, Compiler  } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
// import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ClassesService{
  constructor(private http: Http, 
              // private _router: Router, 
              private _httpClient: HttpClient, 
              private _compiler: Compiler){
  }

  allClassesBySchool(schoolId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/classes/get/school/'+schoolId)
          .map(res => res.json());
  }

  
  allClassesByTeacherId(teacherId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/classes/get/teacher/'+teacherId)
          .map(res => res.json());
  }

  allClassesCountBySchool(schoolId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/classes/count/school/'+schoolId)
          .map(res => res.json());
  }

  getClassById(classId){
      // console.log("USER ID FROM AUTH USER: ", userId);
      return this.http.get('https://mmschoolapp.azurewebsites.net/api/classes/get/'+classId)
          .map(res => res.json());
  }

}

