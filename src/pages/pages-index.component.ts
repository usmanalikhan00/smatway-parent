export {HomePage} from "../pages/home/home";
export {LoginPage} from "../pages/login/login";
export {StudentPerformance, Profile} from "../pages/studentperformance/studentperformance";
export {SingleAssignment} from "../pages/singleassignment/singleassignment";
export {StudentActivity} from "../pages/studentactivity/studentactivity";
export {StudentAssignments} from "../pages/studentassignment/studentassignment";
export {Planner, EventDetail} from "../pages/planner/planner";
export {Inbox, SendMessage} from "../pages/inbox/inbox";
export {SingleChat} from "../pages/singlechat/singlechat";