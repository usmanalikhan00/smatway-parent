import {Component} from "@angular/core";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {NavController, Events, AlertController, ToastController, MenuController} from "ionic-angular";
import { Storage } from '@ionic/storage';
import {LoginService} from '../../services/login.service'
import {StudentsService} from '../../services/students.service'
import {HomePage} from "../pages-index.component";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [LoginService, StudentsService]
})

export class LoginPage {

  loginForm: FormGroup;
  loggedUser: any = null
  isLogged: any = false


  constructor(public nav: NavController, 
              public forgotCtrl: AlertController, 
              public menu: MenuController, 
              public Events: Events, 
              public _formBuilder: FormBuilder, 
              public _loginService: LoginService, 
              public _studentsService: StudentsService, 
              public _storage: Storage, 
              public toastCtrl: ToastController) {
    this.menu.swipeEnable(true);
    this._buildLoginForm()
    // this._storage.remove('user')
  }

  _buildLoginForm(){
    this.loginForm = this._formBuilder.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  ionViewWillEnter(){
    // this._storage.remove('user')
    this._storage.get('user').then(val => {
      if (val){
        console.log("PREVIOUS LOGGED USER:---", JSON.parse(val))
        this.isLogged = true
        // this._storage.set('user', JSON.stringify(val))
        this.nav.setRoot(HomePage)
      }else{
        console.log("NO PREVIOUS USER")
        this.isLogged = false
      }
    })
  }

  // go to register page

  // login and go to home page
  login() {
    this.nav.setRoot(HomePage);
  }

  checkLogin(values){
    this._loginService.checkLogin(values.username, values.password)
    .subscribe(result => {
      if (result.status){
        // console.log("VALUES FROM LOGIN:---\n", result.user)
        this.loggedUser = result.user
        if (this.loggedUser.roleId === parseInt('4')){
          this.loggedUser.role = 'parent'
          // this.loggedUser.schoolId = this.loggedUser.teacher.schoolId
          this._studentsService.getStudentByParentId(this.loggedUser.parent.id)
          .subscribe(result => {
            // console.log("STUDENT FROM PARET:---", result)
            this.loggedUser.student = result.student
            this._storage.set('user', JSON.stringify(this.loggedUser))
            this.Events.publish('user:created', this.loggedUser, Date.now());
            this.presentToast('Your were successfully Logged In')
            this.nav.setRoot(HomePage)
          })
        }else{
          this.presentToast('Username Or Password Incorrect!!')
        }
      }else{
        this.presentToast(`${result.message}`)
      }
    })
  }

  presentToast(msg){
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'center'
    });
    toast.present();    
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
