import { Component } from '@angular/core';
import { NavController, 
         LoadingController, 
         ModalController, 
         Platform, 
         NavParams, 
         ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import * as _ from 'lodash';
import {AssignmentsService} from '../../services/assignment.service'
import { SingleAssignment } from '../pages-index.component';

@Component({
  selector: 'student-assignments',
  templateUrl: 'studentassignment.html',
  providers: [AssignmentsService]
})

export class StudentAssignments {

  loggedUser: any = null
  classId: any = null
  allAssigns: any = null
  selectedStudent: any = null
  loader: any = null

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public _assignmentsService: AssignmentsService,
    private storage: Storage) {
  }

  ionViewWillEnter() {
    let created_at = this.getDate()
    this.storage.get('user').then(val => {
      this.loggedUser = JSON.parse(val)
      this.selectedStudent = this.loggedUser.student
      console.log("LOCAL STORAGE FROM ASSIGNMENTS:---\n", this.loggedUser,)
      this.getAllAssgnments()
    })
  }
  
  getAllAssgnments(){
    this.presentLoading()
    this._assignmentsService.allAssignmentsByStudentId(this.loggedUser.student.id)
    .subscribe(result => {
      console.log("RESULT FROM ALL STUENT ASSIGNMENTS:---\n", result)
      this.allAssigns = result.assignments
      this.loader.dismissAll()
    })
  }

  getDate(){
    let day = new Date().getDate()
    let month = new Date().getMonth()
    let year = new Date().getFullYear()
    let fullDate = null
    let attendanceStudents = []
    if (month+1 < 10){
      fullDate = year+'-0'+(month+1)+'-'+day
    }else{
      fullDate = year+'-'+(month+1)+'-'+day
    }
    let created_at = moment(fullDate, 'YYYY-MM-DD').utcOffset(0, true).format();
    return created_at;
  }


  getAssignment(assignment){
    console.log("ASSIGNMENT TO GET:---\n", assignment)
    this.navCtrl.push(SingleAssignment, {'classId': this.classId, 'assignment': assignment})
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: 'loading…'
    });
    this.loader.present();
  }

}

