import {Component} from "@angular/core";
import {NavController, PopoverController, NavParams, LoadingController, Events} from "ionic-angular";
import {Storage} from '@ionic/storage';
import * as moment from "moment";
import * as _ from "lodash";

import {StudentActivity, 
        Planner, 
        StudentAssignments,
        StudentPerformance} from '../pages-index.component';

import {StudentsService} from '../../services/students.service';
import {ClassesService} from '../../services/classes.service';
import {AssignmentsService} from '../../services/assignment.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html', 
  providers: [StudentsService, ClassesService, AssignmentsService]
})

export class HomePage {
  // search condition
  public search = {
    name: "Rio de Janeiro, Brazil",
    date: new Date().toISOString()
  }

  selectedClass: any = null
  classStudents: any = null
  loggedUser: any = null
  loader: any = null
  presentCount: any = 0
  absentCount: any = 0
  presentTeacherCount: any = 0
  absentTeacherCount: any = 0
  assignmentsCount: any = 0

  allAttendances: any = []

  tab1: any;
  tab2: any;
  tab3: any;
  tab4: any;
  tab5: any;

  toggleTabs: boolean = false;

  rootParams = {
    students: '',
    classId: '',
    attendeStudents: '',
    selectedStudent: ''
  }

  attendeStudents: any = []


  constructor(private storage: Storage, 
              private navParams: NavParams, 
              private _studentsService: StudentsService, 
              private loadingController: LoadingController, 
              private _classesService: ClassesService, 
              private _assignmentsService: AssignmentsService, 
              public nav: NavController, 
              public Events: Events, 
              public popoverCtrl: PopoverController) {
    this.tab1 = StudentActivity
    this.tab2 = StudentAssignments
    this.tab3 = StudentPerformance
  }

  ionViewWillEnter() {
    let created_at = this.getDate()
    // this.presentLoading()
    this.storage.get('user').then((val) => {
      this.loggedUser = JSON.parse(val)
      // this.loader.dismissAll()
    }).catch((err) => {
      console.log(err)
    });
  }

  getDate(){
    let day = new Date().getDate()
    let month = new Date().getMonth()
    let year = new Date().getFullYear()
    let fullDate = null
    let attendanceStudents = []
    if (month+1 < 10){
      fullDate = year+'-0'+(month+1)+'-'+day
    }else{
      fullDate = year+'-'+(month+1)+'-'+day
    }
    let created_at = moment(fullDate, 'YYYY-MM-DD').utcOffset(0, true).format();
    return created_at;
  }

  getMonth(){
    let month = moment().format('M')
    return month;
  }
  
  getYear(){
    let year = moment().format('YYYY')
    return year;
  }


  // getClassAttendanceByDate(created_at){
  //   console.log("OPTION TO FECTCH FOR CLASS TODAY:--", created_at, this.selectedClass.id)
  //   this._classAttendanceService.getTodayClassAttendance(created_at, this.selectedClass.id)
  //   .subscribe(result => {
  //     console.log('RESULT FROM CLASS ATTENDANCE:--\n', result)
  //     var presentCount = 0
  //     var absentCount = 0
  //     if (result.status){
  //       for (let obj of result.attendance){
  //         if (obj.isPresent)
  //           presentCount++
  //         else
  //           absentCount++
  //       }
  //     }
  //     this.presentCount = presentCount
  //     this.absentCount = absentCount
  //   })
  // }

  getClassAssignments(classId){
    this._assignmentsService.allAssignmentsByClassId(classId)
    .subscribe(result => {
      console.log('RESULT FROM CLASS ASSIGNMENTS:--\n', result)
      var assignmentsCount = 0
      var absentCount = 0
      if (result.status){
        this.assignmentsCount = result.assignments.length
      }
    })
  }

  // getTeacherMonthAttendance(teacherId, month, year){
  //   console.log("OPTION TO FECTCH FROM TEACHER MONTH:--", teacherId, month, year)
  //   this._classAttendanceService.getTeacherMonthAttendance(teacherId, month, year)
  //   .subscribe(result => {
  //     var presentCount = 0
  //     var absentCount = 0
  //     if (result.status){
  //       for (let obj of result.attendances){
  //         if (obj.isPresent)
  //           presentCount++
  //         else
  //           absentCount++
  //       }
  //       this.allAttendances = result.attendances
  //     }
  //     console.log('\nRESULT FROM TEACHER MONTH ATTENDANCE:--\n', result, '\n', this.allAttendances)
  //     this.presentTeacherCount = presentCount
  //     this.absentTeacherCount = absentCount

  //   })
  // }

  presentLoading() {
    this.loader = this.loadingController.create({
      content: 'loading…'
    });
    this.loader.present();
  }

}

//
