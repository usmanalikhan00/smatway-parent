import { Component, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { NavController, 
         LoadingController, 
         AlertController, 
         NavParams,
         ModalController, 
         ViewController,  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Calendar } from '@ionic-native/calendar';
import {PlannerService} from '../../services/planner.service';
import {HomePage} from '../../pages/pages-index.component';
import * as _ from 'lodash'

@Component({
  selector: 'all-planner',
  templateUrl: 'planner.html',
  providers: [PlannerService]
})
export class Planner {

  loggedUser: any = null
  allPlanner: any = []
  selectedDay = new Date()
  selectedDate: any;
  selectedObject
  eventSource = []
  viewTitle;
  modal: any;
  loader: any;
  isToday: boolean;
  calendarModes = [
    { key: 'month', value: 'Month' },
    { key: 'week', value: 'Week' },
    { key: 'day', value: 'Day' },
  ]
  calendar = {
    mode: this.calendarModes[0].key,
    currentDate: new Date()
  }; // these are the variable used by the calendar.

  constructor(
    public navCtrl: NavController,
    private _plannerService: PlannerService,
    public loadingController: LoadingController,  
    public modalCtrl: ModalController,  
    private storage: Storage) {
    this.storage.get('user').then(val => {
      this.loggedUser = JSON.parse(val)
      console.log("LOCAL STORAGE FROM EVENTS PLANNER:---", this.loggedUser)
      this.addEvent()  
    })
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
  
  onEventSelected(event) {
    console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
  }
  
  changeMode(mode) {
    this.calendar.mode = mode;
  }
  
  today() {
    this.calendar.currentDate = new Date();
  }
  
  onTimeSelected(ev) {
    console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
    (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled, ev);
    this.selectedObject = ev
  }
  
  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
    this.selectedDay = event

  }
  
  onRangeChanged(ev) {
    console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
  }
  
  markDisabled = (date: Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return (date < current);
  };

  blockDayEvent(date) {
    let startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));

    let endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));

    let events = this.eventSource;
    events.push({
      title: 'All Day ',
      startTime: startTime,
      endTime: endTime,
      allDay: true
    });
    this.eventSource = [];
    setTimeout(() => {
      this.eventSource = events;
    });
  }

  presentLoading() {
    this.loader = this.loadingController.create({
      content: 'loading…'
    });
    this.loader.present();
  }

  addEvent() {
    this.presentLoading()
    this.eventSource = []
    this._plannerService.allPlannerBySchoolId(this.loggedUser.student.schoolId)
    .subscribe(result => {
      let events = this.eventSource;
      let filterPlanner: any = []
      this.allPlanner = result.planners      
      if (!this.loggedUser.student.classId){
        filterPlanner = _.filter(this.allPlanner, {'type': 2})
        for (let obj of filterPlanner){
          var event = {
            startTime: new Date(obj.startDate),
            endTime: new Date(obj.endDate),
            allDay: false,
            title: obj.title,
            type: obj.type,
            classes: obj.classes
          };
          events.push(event)
        }
      }else{
        for (let obj of result.planners){
          var event = {
            startTime: new Date(obj.startDate),
            endTime: new Date(obj.endDate),
            allDay: false,
            title: obj.title,
            type: obj.type,
            classes: obj.classes
          };
          events.push(event)
        }
      }
      console.log("RESULT FROM SCHOOL PLANNER:---", result, '\n', events)
      this.eventSource = [];
      setTimeout(() => {
        this.eventSource = events;
        this.loader.dismissAll()
      });
    })
  }

  onOptionSelected($event: any) {
    console.log($event)
    //this.calendar.mode = $event
  }

  eventSelected(value){
    console.log("EVENT SELECTED:--", value)
    let modal = this.modalCtrl.create(EventDetail, { 'event': value });
    modal.present();
  }
}


@Component({
  selector: 'event-detail',
  template: `
  <!-- <ion-header>
    <ion-navbar>
      <ion-buttons start class="m-t-10">
        <button (click)="dismiss()" style="margin-right:10px;">
          <ion-icon name='close'></ion-icon>
        </button>
      </ion-buttons>
      <ion-title class="m-t-10">
        {{selectedEvent.title}}
      </ion-title>
    </ion-navbar>
  </ion-header> -->
  <ion-header>
    <ion-navbar color="primary">
      <ion-buttons start class="m-t-10">
        <button (click)="dismiss()" style="margin-right:10px;">
          <ion-icon name='close'></ion-icon>
        </button>
      </ion-buttons>
      <ion-title>{{selectedEvent.title}}</ion-title>
    </ion-navbar>
  </ion-header>
  <ion-content>
    <ion-row>
      <ion-col col-12 padding>
        <span class="header-title text-primary">START DATE</span>
        <br>
        <span>{{selectedEvent.startTime | date:'fullDate'}}</span>
        <br>
        <span>{{selectedEvent.startTime | date:'HH:mm a'}}</span>

      </ion-col>
      <ion-col col-12 padding>
        <span class="header-title text-primary">END DATE</span>
        <br>
        <span>{{selectedEvent.endTime | date:'fullDate'}}</span>
        <br>
        <span>{{selectedEvent.endTime | date:'HH:mm a'}}</span>
      </ion-col>
      <ion-col col-12 padding>
        <span class="header-title text-primary">Type</span>
        <br>
        <span *ngIf="selectedEvent.type === 1">Time Table</span>
        <span *ngIf="selectedEvent.type === 2">Event</span>
      </ion-col>
      <ion-col col-12 padding>
        <span class="header-title text-primary">Class(es)</span>
        <div class="classItems">
          <ion-chip color="light" *ngFor="let obj of selectedEvent.classes">
            <ion-label>{{obj.name}}</ion-label>
          </ion-chip>  
        </div>
      </ion-col>

    </ion-row>
  </ion-content>`
})

export class EventDetail {

  selectedEvent: any = null
  classes: any = null

  constructor(public navParams: NavParams,
              public viewCtrl: ViewController) {
    this.selectedEvent = navParams.get('event')
    console.log('EVENT FROM MODAL:---\n', this.selectedEvent);
    if (this.selectedEvent.classes.length){
      this.classes = this.selectedEvent.classes[0]
    }
  }

  dismiss(){
    this.viewCtrl.dismiss()
  }
}